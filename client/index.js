const socket = io("http://localhost:8080");

const messageform = document.querySelector(".chatbox form");
const messageList = document.querySelector("#messagelist");
const userList = document.querySelector("ul#users");
const chatboxinput = document.querySelector(".chatbox input");
const useraddform = document.querySelector(".modal");
const backdrop = document.querySelector(".backdrop");
const useraddinput = document.querySelector(".modal input");

const messages = [];
let users = [];

//Socket Listeners
socket.on("message_client", (message) => {
  messages.push(message);
  updateMessage();
});

socket.on("users", (_users) => {
  users = _users;
  updateUsers();
});

//Event Listeners
messageform.addEventListener("submit", messageSubmitHandler);
useraddform.addEventListener("submit", userAddHandler);

//Functions
function messageSubmitHandler(e) {
  e.preventDefault();
  let message = chatboxinput.value;
  if (!message) {
    return alert("Empty Message");
  }

  socket.emit("message", message);
  chatboxinput.value = "";
}

function updateMessage() {
  messageList.textContent = "";

  for (let i = 0; i < messages.length; i++) {
    messageList.innerHTML += `<li>
                                    <p>${messages[i].user}</p>
                                    <p>${messages[i].message}</p>
                               </li>`;
  }
}

function updateUsers() {
  userList.textContent = "";

  for (let i = 0; i < users.length; i++) {
    let node = document.createElement("LI");
    let textnode = document.createTextNode(users[i]);
    node.appendChild(textnode);
    userList.appendChild(node);
  }
}

function userAddHandler(e) {
  e.preventDefault();
  let username = useraddinput.value;

  if (!username) {
    return alert("Must Add a Username");
  }

  socket.emit("adduser", username);
  useraddform.classList.add("disappear");
  backdrop.classList.add("disappear");
}
